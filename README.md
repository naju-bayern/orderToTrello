# Order to Trello

## Setup

### Backend
copy the following lines to yout docker-compose.yml:
```
nforder:
    build: /srv/docker/nfOrder/
    networks:
     - public
    environment:
     - TRELLO_KEY=
     - TRELLO_TOKEN=
     - TRELLO_LIST=
```

next, set the three Environement Variables and run `docker-compose up -d nforder`

Now you have to enshure, the container is reachable over tzhe internet. If you use a nginx reverse proxy to access yout docker containers you could use:
```
location /nforder/ {
    rewrite ^/nforder/(.*) /$1 break;
    proxy_pass       http://nforder:9000;
    proxy_set_header Host      $host;
    proxy_set_header X-Real-IP $remote_addr;
}
```

### Frontend
Now you can add a form to your web-frontend:

an Example can be fund in exampleForm.html

dont forget to create an "Order Complete"-Page and set it's link in the hidden input field `backlink`. After adding the card the container will send the user back to this address.

