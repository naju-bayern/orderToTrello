FROM ubuntu:latest

RUN apt-get update && apt-get install -y cron wget nodejs npm zip coreutils
RUN cd /root/ && npm install node-trello express
 
ADD order.js /root/
ADD run.sh /root/
CMD /root/run.sh
