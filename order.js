if(!process.env.hasOwnProperty("TRELLO_KEY")){
  console.log("Please set the environement variable TRELLO_KEY with your Trello-API-KEY!");
  process.exit(1);
}

if(!process.env.hasOwnProperty("TRELLO_TOKEN")){
  console.log("Please set the environement variable TRELLO_TOKEN with your Trello-API-TOKEN!");
  process.exit(1);
}

if(!process.env.hasOwnProperty("TRELLO_LIST")){
  console.log("Please set the environement variable TRELLO_LIST to the ID of the List to add the Cards!");
  process.exit(1);
}


var Trello = require("node-trello");
var t = new Trello(process.env.TRELLO_KEY, process.env.TRELLO_TOKEN);


var express = require('express');
var app = express();

app.get('/', function (req, res) {
  var ret = "";
  for(var key in req.query){
	if(key == "backlink" || key == "name" || key == "street" || key == "city" || req.query[key] == "" || req.query[key] == 0)
	 continue;
	ret += key + ": " + req.query[key] + "\n\r";
  }
  creatCard("Material verschicken",ret + "\n \n an: \n" + req.query.name + "\n" + req.query.street + "\n" + req.query.city);
  res.redirect(req.query.backlink);
});

app.listen(9000, function () {
});


function creatCard(name,desc){
var newCard = 
          {name: name, 
           desc: desc,
          pos: "top", 
          idList: process.env.TRELLO_LIST
          };

t.post('/1/cards/', newCard, 
            function(msg){
                console.log("card added" + msg  );
            }, 
            function() {
                 console.log("error adding card");
            }
           );
}
